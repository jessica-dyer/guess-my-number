import random

# Take inputs
lower = int(input("Enter lower bound: "))
upper = int(input("Enter upper bound: "))

string_lower = str(lower)
string_upper = str(upper)

x = random.randint(lower, upper)
guess = None
string_message = "Guess a number between " + string_lower + " and " + string_upper + ": "

while guess != x:
  guess = input(string_message)
  guess = int(guess)

  if guess == x:
    print("Congratulations! You won!")
    break
  elif guess < x:
    print("Your guess is too low. Try again!")
  elif guess > x:
    print("Your guess is too high. Try again!")

import random

def average(low, high):
  average = int((low + high) / 2)
  return average

input("Please choose a number between 1 and 50. Remember your number! Press enter when ready.\n")

low = 1
high = 50
guess = average(low, high)

command = input("\nIs " + str(guess) + " your number? (correct, high, low)")

while command != "correct":
  if command == "correct":
    break
  elif command == "high":
    print("I've guessed too high!")
    high = guess
    guess = average(low, high)
    command = input("\nIs " + str(guess) + " your number? (correct, high, low)")
  elif command == "low":
    print("I've guessed too low!")
    if high - guess == 1:
      guess = high
    else:
      low = guess
      guess = average(low, high)
    command = input("\nIs " + str(guess) + " your number? (correct, high, low)")

print("I guessed your number!")


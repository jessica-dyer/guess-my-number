import random
import math

# Taking inputs
lower = int(input("Enter lower bound:- "))
upper = int(input("Enter upper bound:- "))

# generate a random number between the lower
# and upper bounds
x = random.randint(lower, upper)
guesses = round(math.log(upper - lower + 1, 2))
print("You've only got ",
      guesses, " chances to guess the integer!")

# Initialize number of guesses.
count = 0

# for calculation of minimum number of
# guesses depends upon range
while count < guesses:
      count += 1

      guess = int(input("Guess a number: "))

      # Test
      if x == guess:
            print("Nice job! You guessed the number in ", count, "tries!")
            break
      elif x > guess:
            print("You guessed too small!")
      elif x < guess:
            print("You guessed too high!")

if count >= guesses:
      print("The number is ", x)
      print("Better luck next time!")
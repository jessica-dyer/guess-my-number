message = "This is a string in a Python file."
some_number = 1928
print("The message is", message)
print("The number is", some_number)

number_three = 3
string_three = "3"
print(number_three)
print(string_three)

number_three = str(number_three)
print(type(number_three))